<?php 

class Products_controller_1900016083 extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('products_1900016083');
    }

    public function getProduct(){
        $data['getPoduct'] = $this->products_1900016083->getProducts();
        $this->load->view('client', $data);
    }

    public function postProduct(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->products_1900016083->addProduct($data);
    }

    public function deleteProduct( $id_product = NULL ){
        $data = array('id_product' => $id_product );
        $this->products_1900016083->deleteProduct($data);
    }

    public function putProduct( $id_barang = NULL ){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'id_product' => $id_product,
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->products_1900016083->updateProduct($data);
    }
}


?>